import classes.*;

public class Main {
    public static void main(String[] args){
        abstraction();
        encapsulation();
        inheritance();
        polymorphism();
    }

    private static void abstraction(){
        // A voice controlled lcd television is definitely a complex system, however it's really simple to use it. Turn it on and see its stauts.
        System.out.println("Abstraction example");
        VoiceControlledLcdTelevision voiceControlledLcdTelevision = new VoiceControlledLcdTelevision();
        voiceControlledLcdTelevision.turnOn();
        System.out.println("Old television status is: " + voiceControlledLcdTelevision.getStatus());
        voiceControlledLcdTelevision.sendNewCommand("ON");
        System.out.println("Old television status is: " + voiceControlledLcdTelevision.getStatus());
        voiceControlledLcdTelevision.sendNewCommand("AARON LEARNING JAVA");
    }

    private static void encapsulation(){
        // We dont care about whats inside our old television, we just want to create it, turn it on and see its status :)
        // Old tvs has transistors inside, but to use it, we dont need to know it.
        System.out.println("Encapuslation example");
        OldTelevision oldTv = new OldTelevision();
        // oldTv.getTransitors
        System.out.println("I cannot access or modify old television transistors!");
    }

    private static void inheritance(){
        // We have created a voice controlled lcd television without the needed of create all the lcd television logic again
        System.out.println("Inheritance example");
        VoiceControlledLcdTelevision voiceControlledLcdTelevision = new VoiceControlledLcdTelevision();
        System.out.println("Initial status of TV: " + voiceControlledLcdTelevision.getStatus());
        voiceControlledLcdTelevision.sendNewCommand("ON");
        System.out.println("Final status of TV: " + voiceControlledLcdTelevision.getStatus());
    }

    private static void polymorphism(){
        System.out.println("Polymorphism example");
        // We dont care which television is, we just want to turn it on and know its status :)
        // Create them
        Television tv1 = new OldTelevision();
        Television tv2 = new LcdTelevision();
        Television tv3 = new VoiceControlledLcdTelevision();
        OldTelevision oldTelevision = new OldTelevision();
        oldTelevision.turnOn();
        OldTelevision oldTelevision2 = new OldTelevision();
        oldTelevision2.turnOn();
        new RemoteController(tv1);



    }
}
