package classes;

import java.util.ArrayList;
import java.util.List;

public class OldTelevision extends Television {

    private List<Integer> transistors;

    public OldTelevision() {
        super(10, 144);
        transistors = new ArrayList<>(100);
        for (Integer transistor : this.transistors){
            transistor = 0;
        }
    }

    @Override
    public void turnOn(){
        super.turnOn();
        for (int i = 0; i < transistors.size(); i++){
            Integer transistor = this.transistors.get(i);
            transistor = this.transistors.size() % i;
        }
    }

    @Override
    public void turnOff(){
        super.turnOff();
        for (Integer transistor : this.transistors){
            transistor = 0;
        }
    }
    public static String getYears() {
        return "1880-1890";
    }
}
