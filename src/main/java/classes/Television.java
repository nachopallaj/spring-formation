package classes;

public abstract class Television {
    private int numOfChannels;
    private int frequency;
    private boolean isOn;

    public Television(int numOfChannels, int frequency){
        this.numOfChannels = numOfChannels;
        this.frequency = frequency;
        this.isOn = false;
    }

    public void turnOn(){
        this.isOn = true;
    }

    public void turnOff(){
        this.isOn = false;
    }

    public boolean isOn(){
        return this.isOn;
    }

    public String getStatus() {
        return this.isOn() ? "ON" : "OFF";
    }
}
