package classes;

public class VoiceControlledLcdTelevision extends LcdTelevision {

    public void sendNewCommand(String voiceCommand){
        if (voiceCommand.equals("ON")){
            this.turnOn();
        } else if (voiceCommand.equals("OFF")){
            this.turnOff();
        } else {
            System.out.println("Sorry, I cannot recognize the command");
        }
    }
}
