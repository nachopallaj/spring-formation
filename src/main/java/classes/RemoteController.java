package classes;

public class RemoteController {
    private final Television television;

    public RemoteController(Television tv){
        this.television = tv;
    }

    public void pressTurnOnButton(){
        this.television.turnOn();
    }

    public void pressTurnOffButton(){
        this.television.turnOff();
    }
}
