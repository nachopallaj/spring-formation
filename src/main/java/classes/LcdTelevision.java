package classes;

import java.util.ArrayList;
import java.util.List;

public class LcdTelevision implements Television {

    private List<Boolean> leds;

    public LcdTelevision() {
        this.leds = new ArrayList<>(200);
        for (Boolean led : leds){
            led = false;
        }
    }

    public void turnOff() {
        for (Boolean led : this.leds){
            led = false;
        }
    }

    @Override
    public boolean isOn() {
        return false;
    }

    @Override
    public String getStatus() {
        return null;
    }

    public void turnOn(){
        for (Boolean led : leds){
            led = true;
        }
    }
}
